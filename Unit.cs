﻿using System;
using System.Collections.Generic;
using IrrlichtLime;
using IrrlichtLime.Core;
using IrrlichtLime.Scene;
using IrrlichtLime.Video;

namespace Cronikles_Skeleton_War
{
    public class Unit //This is the base class for everything that can move and die.
    {
        protected GameState game;
        public AnimatedMeshSceneNode node;
        public Vector3Df position;
        public Vector2Di mapPosition;
        public float angle;
        public float newAngle;
        public int health = 100;
        public int damage = 5;
        public float speed = 0.05f;
        protected Vector3Df rotation = new Vector3Df();
        protected float groundOffset = 0.1f;
        public string state = "idle";
        protected string pstate = "idle";
        public List<Node> path;
        public int pathIndex;
        public float direction;
        int waitTimer = 0;
        public Vector3Df lastPosition;
        Vector2Di avoid;
        protected Vector2Di dest; //For pathfinding movement
        public Vector2Di target = new Vector2Di();  //For direct movement
        public bool killMe = false;
        int dTimer = 0;
        public Unit(GameState g)
        {
            game = g;
            state = "idle";
            dest = new Vector2Di();
        }
        ~Unit()
        {
            //if (node != null) node.Remove();
        }
        public virtual void PreUpdate()
        {
            mapPosition.X = (int)Math.Floor(position.X);
            mapPosition.Y = (int)Math.Floor(position.Z);
            game.gameMap.tiles[mapPosition.X, mapPosition.Y].occupied = true;
            game.gameMap.tiles[mapPosition.X, mapPosition.Y].occupant = this;
        }
        public virtual void PostUpdate()
        {
            if (killMe)
            {
                if (state != "walk" && state != "walk2") game.gameMap.tiles[mapPosition.X, mapPosition.Y].blocked = false;
                game.units.Remove(this);
                node.Remove();
            }
        }
        public virtual void Update()
        {
            if (health <= 0 && state != "die") ChangeState("die");
            switch (state)
            {
                case "attack":
                case "idle":
                    game.gameMap.tiles[mapPosition.X, mapPosition.Y].blocked = true;
                    break;
                case "walk": //This should probably be called "pathfind", but whatever.
                    if (path != null)
                    {
                        if (path.Count <= 0)
                        {
                            ChangeState("idle");
                            break;
                        }
                        if (pathIndex >= 0)
                        {
                            direction = (float)Math.Atan2(position.Z - path[pathIndex].cy, position.X - path[pathIndex].cx) * Utility.toDegrees;
                            position.X -= (float)Math.Cos(direction * Utility.toRadians) * speed;
                            position.Z -= (float)Math.Sin(direction * Utility.toRadians) * speed;
                            if (Math.Abs(position.X - path[pathIndex].cx) < 0.1f && Math.Abs(position.Z - path[pathIndex].cy) < 0.1f)
                            {
                                position.X = path[pathIndex].cx;
                                position.Z = path[pathIndex].cy;
                                pathIndex -= 1;
                                if (pathIndex >= 0)
                                {
                                    newAngle = -(float)Math.Atan2(position.Z - path[pathIndex].cy, position.X - path[pathIndex].cx) * Utility.toDegrees;
                                    newAngle += 90;
                                }
                                else
                                {
                                    pathIndex = 0;
                                    ChangeState("idle");
                                    OnPathEnd();
                                }
                            }
                        }
                        else
                        {
                            pathIndex = 0;
                            ChangeState("idle");
                            OnPathEnd();
                        }
                    }
                    else
                    {
                        ChangeState("wait");
                    }
                    break;
                case "wait":
                    SnapToTile();
                    game.gameMap.tiles[mapPosition.X, mapPosition.Y].blocked = true;
                    waitTimer += 1;
                    if (waitTimer > 7)
                    {
                        MoveTo(dest.X, dest.Y, avoid.X, avoid.Y, true);
                        waitTimer = 0;
                    }
                    break;
                case "walk2":
                    direction = (float)Math.Atan2(position.Z - (target.Y + 0.5f), position.X - (target.X + 0.5f)) * Utility.toDegrees;
                    position.X -= (float)Math.Cos(direction * Utility.toRadians) * speed;
                    position.Z -= (float)Math.Sin(direction * Utility.toRadians) * speed;
                    if (Math.Abs(position.X - (target.X + 0.5f)) < 0.1f && Math.Abs(position.Z - (target.Y + 0.5f)) < 0.1f)
                    {
                        position.X = target.X + 0.5f;
                        position.Z = target.Y + 0.5f;
                        ChangeState("idle");
                    }
                    break;
                case "die":
                    dTimer += 1;
                    if (dTimer > 100) position.Y -= 0.01f;
                    if (dTimer > 150) killMe = true;
                    break;
            }

            if (state == "walk") DoCollisions();
            if (newAngle > 360.0f) newAngle -= 360.0f;
            if (newAngle < 0.0f) newAngle += 360.0f;
            if (angle > 360.0f) angle -= 360.0f;
            if (angle < 0.0f) angle += 360.0f;
            if (angle != newAngle)
            {
                float deltaAngle = newAngle - angle;
                if (Math.Abs(deltaAngle) > 180) deltaAngle = -deltaAngle;
                if (Math.Abs(deltaAngle) >= 10.0f)
                {
                    angle += 10.0f * Math.Sign(deltaAngle);
                }
                else
                {
                    angle = newAngle;
                }
            }
            lastPosition = node.Position;
            if (state != "die") position.Y = game.gameMap.getHeightAt(position.X, position.Z) + groundOffset;
            node.Position = position;
            rotation.X = 0; rotation.Z = 0;
            rotation.Y = angle;
            node.Rotation = rotation;
        }
        public void SnapToTile()
        {
            float cx = mapPosition.X + 0.5f;
            float cy = mapPosition.Y + 0.5f;
            if (position.X != cx)
            {
                position.X = cx;
            }
            if (position.Y != cy)
            {
                position.Y = cy;
            }
        }
        public void DoCollisions()
        {
            for (int i = 0; i < game.units.Count; i++)
            {
                if (game.units[i] != this)
                {
                    if (state == "walk")
                    {
                        if (game.units[i].mapPosition.X == path[pathIndex].x && game.units[i].mapPosition.Y == path[pathIndex].y)
                        {
                            OnUnitHit(game.units[i]);
                        }
                        if (game.units[i].state == "walk")
                        {
                            if (game.units[i].path != null && path != null)
                            {
                                if (game.units[i].path.Count > 0 && path.Count > 0)
                                {
                                    Node n = game.units[i].path[game.units[i].pathIndex];
                                    if (n != null)
                                    {
                                        if (path[pathIndex] == n && avoid != game.units[i].mapPosition)
                                        {
                                            OnUnitHit(game.units[i]);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        public int MoveTo(int x, int y, int avoidx=-1, int avoidy = -1, bool avoidOthers = false)
        {
            bool hugh_mungus = false;
            if (this is Skeleton)
            {
                game.skelPathCnt += 1;
                if (game.skelPathCnt > 7)
                {
                    hugh_mungus = true;
                }
            }
            else if (this is Human)
            {
                game.humPathCnt += 1;
                if (game.humPathCnt > 7)
                {
                    hugh_mungus = true;
                }
            }
            dest.X = x;
            dest.Y = y;
            int error=0;
            if (hugh_mungus)
            {
                path = null;
                error = 1;
            }
            else
            {
                path = game.gameMap.FindPathTo(mapPosition.X, mapPosition.Y, x, y, ref error, avoidx, avoidy, avoidOthers);
            }
            if (error == 4)
            {
                ChangeState("wait");
            }
            if (path == null)
            {
                if (error == 1)
                {
                    ChangeState("wait");
                }
                else
                {
                    ChangeState("idle");
                }
                return error;
            }
            pathIndex = path.Count - 1;
            if (path.Count <= 0)
            {
                pathIndex = 0;
            }
            else
            {
                newAngle = -(float)Math.Atan2(position.Z - path[pathIndex].cy, position.X - path[pathIndex].cx) * Utility.toDegrees;
                newAngle += 90;

            }
            if (error == 0 || error == 3) ChangeState("walk");
            if (error != 0) Console.WriteLine("OH SHIT " + error);
            return error;
        }
        public virtual void OnPathEnd()
        {

        }
        public virtual void OnUnitHit(Unit perpetrator)
        {
            pathIndex += 1;
            if (pathIndex > path.Count - 1)
            {
                pathIndex = path.Count - 1;
            }
            MoveTo(dest.X, dest.Y, perpetrator.mapPosition.X, perpetrator.mapPosition.Y, true);
            avoid = perpetrator.mapPosition;
        }
        public void ChangeState(string newState)
        {
            mapPosition.X = (int)Math.Floor(position.X);
            mapPosition.Y = (int)Math.Floor(position.Z);
            if (newState == "walk" && state != "walk") //If we're beginning to move then unblock the tile we're on
            {
                game.gameMap.tiles[mapPosition.X, mapPosition.Y].blocked = false;
            }
            if (newState == "idle" && state != "idle")
            {
                if (position.X != mapPosition.X + 0.5f) position.X = mapPosition.X + 0.5f;
                if (position.Z != mapPosition.Y + 0.5f) position.Z = mapPosition.Y + 0.5f;
            }
            state = newState;
        }
        public virtual void Damage(int amount, Unit perpetrator)
        {
            health -= amount;
        }
        public void FaceUnit(Unit other)
        {
            newAngle = -(float)Math.Atan2(position.Z - other.position.Z, position.X - other.position.X) * Utility.toDegrees;
            newAngle += 90;
        }
    }
}