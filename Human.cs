﻿using System;
using System.Collections.Generic;
using IrrlichtLime;
using IrrlichtLime.Core;
using IrrlichtLime.Scene;
using IrrlichtLime.Video;

namespace Cronikles_Skeleton_War
{
    public class Human : Unit
    {
        public string aiState = "wander";
        static Vector3Df scale = new Vector3Df(0.15f, 0.15f, 0.15f);
        List<Node> bleh = new List<Node>();
        protected Skeleton chaseTarget = null;
        int attackTimer;
        public Human(GameState g) : base(g)
        {
            node = g.sceneManager.AddAnimatedMeshSceneNode(Assets.animModels["human"]);
            node.Scale = scale;
            node.SetFrameLoop(0, 59);
            node.ID = GameState.ENEMY;

            newAngle = angle;
        }
        public override void Update()
        {
            base.Update();
            if (state != "die")
            {
                switch (aiState)
                {
                    case "wander":
                        if (game.gameMap.tiles[target.X, target.Y].occupied && game.gameMap.tiles[target.X, target.Y].occupant != this)
                        {
                            ChangeState("idle");
                        }
                        if (state == "idle")
                        {
                            bleh.Clear();
                            int dx = 0, dy = 0;
                            for (int i = 0; i < 8; i++)
                            {
                                switch (i)
                                {
                                    case 0:
                                        dx = 1;
                                        dy = 0;
                                        break;
                                    case 1:
                                        dx = 1;
                                        dy = 1;
                                        break;
                                    case 2:
                                        dx = 0;
                                        dy = 1;
                                        break;
                                    case 3:
                                        dx = -1;
                                        dy = 1;
                                        break;
                                    case 4:
                                        dx = -1;
                                        dy = 0;
                                        break;
                                    case 5:
                                        dx = -1;
                                        dy = -1;
                                        break;
                                    case 6:
                                        dx = 0;
                                        dy = -1;
                                        break;
                                    case 7:
                                        dx = 1;
                                        dy = -1;
                                        break;
                                }
                                Node n = game.gameMap.tiles[Math.Max(0, Math.Min(game.gameMap.width - 1, mapPosition.X + dx)), Math.Max(0, Math.Min(game.gameMap.height - 1, mapPosition.Y + dy))];
                                if (!n.blocked && !n.occupied)
                                {
                                    bleh.Add(n);
                                }

                            }
                            if (bleh.Count == 0)
                            {
                                ChangeState("idle");
                                break;
                            }
                            int r = Program.random.Next(bleh.Count);
                            target.X = bleh[r].x;
                            target.Y = bleh[r].y;
                            newAngle = -(float)Math.Atan2(position.Z - (target.Y + 0.5f), position.X - (target.X + 0.5f)) * Utility.toDegrees;
                            newAngle += 90;
                            if (state != "walk2") ChangeState("walk2");
                        }
                        float nDis = 999999.0f;
                        Skeleton nSkel = null;
                        for (int i = 0; i < game.skeletons.Count; i++)
                        {
                            float dis = Utility.ManhattanDistance(position.X, position.Z, game.skeletons[i].position.X, game.skeletons[i].position.Z);
                            if (dis < 8.0f && dis < nDis && game.skeletons[i].state != "die" && game.skeletons[i].killMe == false)
                            {
                                nDis = dis;
                                nSkel = game.skeletons[i];
                            }
                        }
                        if (nSkel != null)
                        {
                            aiState = "attack";
                            chaseTarget = nSkel;
                            MoveTo(chaseTarget.mapPosition.X, chaseTarget.mapPosition.Y, -1, -1, true);
                        }
                        break;
                    case "attack":
                        if (chaseTarget == null)
                        {
                            aiState = "wander";
                            if (state == "attack")
                                ChangeState("idle");
                        }
                        else
                        {
                            if (chaseTarget.killMe || chaseTarget.state == "die")
                            {
                                chaseTarget = null;
                                break;
                            }
                            float xd = Math.Abs(chaseTarget.mapPosition.X - mapPosition.X);
                            float yd = Math.Abs(chaseTarget.mapPosition.Y - mapPosition.Y);
                            if (xd <= 1.0f && yd <= 1.0f && state != "attack" && position.X == mapPosition.X + 0.5f && position.Z == mapPosition.Y + 0.5f)
                            {
                                ChangeState("attack");
                            }
                            if ((xd > 1.0f || yd > 1.0f) && state != "walk")
                            {
                                MoveTo(chaseTarget.mapPosition.X, chaseTarget.mapPosition.Y, -1, -1, true);

                                //PROBLEM CODE HERE

                                //Make them move on to another target if current target is unreachable.   
                                if (path != null)
                                {
                                    if (path.Count > 0)
                                    {
                                        if (Math.Abs(path[0].x - chaseTarget.mapPosition.X) > 1.0f || Math.Abs(path[0].y - chaseTarget.mapPosition.Y) > 1.0f)
                                        {
                                            int fuckIt = Program.random.Next(0, game.skeletons.Count);
                                            if (game.skeletons[fuckIt].killMe == false)
                                            {
                                                chaseTarget = game.skeletons[fuckIt];
                                            }
                                            //chaseTarget = null;
                                            break;
                                        }
                                    }
                                }
                            }
                            if (state == "attack")
                            {
                                newAngle = -(float)Math.Atan2(position.Z - chaseTarget.position.Z, position.X - chaseTarget.position.X) * Utility.toDegrees;
                                newAngle += 90;
                                attackTimer += 1;
                                if (attackTimer > 25)
                                {
                                    attackTimer = 0;
                                    chaseTarget.Damage(damage, this);
                                    if (chaseTarget.health <= 0) chaseTarget = null;
                                }
                            }
                        }
                        break;
                }
            }
            else
            {
                if (node.CurrentFrame >= node.EndFrame - 2)
                {
                    node.CurrentFrame = node.EndFrame - 2;
                    node.AnimationSpeed = 0;
                }
            }
            if (state == "idle")
            {
                if (pstate != "idle") node.SetFrameLoop(0, 59);
            }
            else if (state == "attack")
            {
                if (pstate != "attack") node.SetFrameLoop(110, 149);
            }
            else if (state == "die")
            {
                if (pstate != "die") node.SetFrameLoop(154, 189);
            }
            else if (lastPosition != position)
            {
                if (node.StartFrame != 69) node.SetFrameLoop(69, 109); //Walk
            }
            else
            {
                if (node.StartFrame != 0) node.SetFrameLoop(0, 59);
            }
            //155-190
            pstate = state;
        }
        public override void OnUnitHit(Unit perpetrator)
        {
            base.OnUnitHit(perpetrator);
        }
        public override void OnPathEnd()
        {
            if (aiState == "wander")
            {
                
            }
        }
    }
}
