﻿using System;
using System.Collections.Generic;
using IrrlichtLime;
using IrrlichtLime.Core;
using IrrlichtLime.Scene;
using IrrlichtLime.Video;
using IrrlichtLime.GUI;
using System.Diagnostics;

namespace Cronikles_Skeleton_War
{
    class Program
    {
        
        public static bool KEY_LEFT;
        public static bool KEY_RIGHT;
        public static bool KEY_UP;
        public static bool KEY_DOWN;
        public static bool KEY_SHIFT;
        public static bool KEY_Q;
        public static bool KEY_E;
        public static bool KEY_Y;
        public static bool KEY_H;
        public static bool MOUSE_LEFT;
        public static bool MOUSE_RIGHT;
        public static bool MOUSE_LEFT_CLICK;
        public static bool MOUSE_RIGHT_CLICK;
        public static int MOUSE_X;
        public static int MOUSE_Y;
        public static Random random;
        public static State currentState;
        const float FPSCAP = (1.0f / 60.0f) * 1000.0f;
        static void Main()
        {
            IrrlichtDevice device = IrrlichtDevice.CreateDevice(DriverType.Direct3D9, new Dimension2Di(800, 600));
            device.SetWindowCaption("The Cronikles: Skeleton War");
            
            long lastMillisecs = 0;
            long timePassed = 0;
            bool exit = false;
            random = new Random(DateTime.Now.Millisecond);
            Assets.LoadAssets(device);
            currentState = new GameState(device, device.SceneManager, device.GUIEnvironment, device.VideoDriver);
            currentState.Init();
            device.OnEvent += new IrrlichtDevice.EventHandler(device_OnEvent);
            //DateTime is proving to be unreliable. Using stopwatches instead!
            Stopwatch stopwatch = Stopwatch.StartNew();
            while (!exit)
            {
                exit = !device.Run();
                long now = stopwatch.ElapsedMilliseconds;
                timePassed += Math.Abs(now - lastMillisecs);
                if (timePassed > FPSCAP)
                {
                    currentState.Update();
                    currentState.Render();
                    timePassed = 0;
                    MOUSE_LEFT_CLICK = false;
                    MOUSE_RIGHT_CLICK = false;
                }
                lastMillisecs = now;
            }
            stopwatch.Stop();
            device.Dispose();
        }
        static bool device_OnEvent(Event e)
        {
            if (e.Type == EventType.GUI)
            {
                currentState.HandleEvent(e);
            }
            if (e.Type == EventType.Mouse)
            {
                switch (e.Mouse.Type)
                {
                    case MouseEventType.LeftDown:
                        MOUSE_LEFT = true;
                        break;
                    case MouseEventType.LeftUp:
                        MOUSE_LEFT_CLICK = true;
                        MOUSE_LEFT = false;
                        break;
                    case MouseEventType.RightDown:
                        MOUSE_RIGHT = true;
                        break;
                    case MouseEventType.RightUp:
                        MOUSE_RIGHT_CLICK = true;
                        MOUSE_RIGHT = false;
                        break;
                }
                MOUSE_X = e.Mouse.X;
                MOUSE_Y = e.Mouse.Y;
            }
            if (e.Type == EventType.Key)
            {
                if (e.Key.Key == KeyCode.Right || e.Key.Key == KeyCode.KeyD)
                {
                    KEY_RIGHT = e.Key.PressedDown;
                }
                if (e.Key.Key == KeyCode.Left || e.Key.Key == KeyCode.KeyA)
                {
                    KEY_LEFT = e.Key.PressedDown;
                }
                if (e.Key.Key == KeyCode.Up || e.Key.Key == KeyCode.KeyW)
                {
                    KEY_UP = e.Key.PressedDown;
                }
                if (e.Key.Key == KeyCode.Down || e.Key.Key == KeyCode.KeyS)
                {
                    KEY_DOWN = e.Key.PressedDown;
                }
                if (e.Key.Key == KeyCode.LShift || e.Key.Key == KeyCode.RShift)
                {
                    KEY_SHIFT = e.Key.PressedDown;
                }
                if (e.Key.Key == KeyCode.KeyQ || e.Key.Key == KeyCode.Comma)
                {
                    KEY_Q = e.Key.PressedDown;
                }
                if (e.Key.Key == KeyCode.KeyH)
                {
                    KEY_H = e.Key.PressedDown;
                }
                if (e.Key.Key == KeyCode.KeyE || e.Key.Key == KeyCode.Period)
                {
                    KEY_E = e.Key.PressedDown;
                }
                if (e.Key.Key == KeyCode.KeyY)
                {
                    KEY_Y = e.Key.PressedDown;
                }
            }
            return false;
        }
        
    }
}
