﻿using System;
using System.Collections.Generic;
using IrrlichtLime;
using IrrlichtLime.Core;
using IrrlichtLime.Scene;
using IrrlichtLime.Video;
using IrrlichtLime.GUI;

namespace Cronikles_Skeleton_War
{
    public class Node
    {
        public int x, y, h, g, f;
        public float cx, cy;
        public bool blocked; //Tells the pathfinder not to go here
        public bool occupied;
        public Unit occupant;
        public int type;
        public Node parent;
        public bool inClosed, inOpen = false;
        public Node(int xx, int yy, int hh, int gg, Node p)
        {
            parent = p;
            x = xx;
            y = yy;
            g = gg;
            h = hh;
            f = hh + gg;
        }
    }
    public class Map
    {
        Color graveyardColor = new Color(128, 128, 128);
        Color sandColor = new Color(0, 255, 0);
        Color shadowColor = new Color(160, 160, 160);
        
        private IrrlichtDevice device;
        protected GameState game;
        public StaticMesh terrainMesh;
        public Node[,] tiles;
        public int width, height;
        Vertex3D[,] vertices;
        ushort[,,] indices;

        float noiseOffset = 0.0f;
        float noiseScale = 0.1f;
        float noiseHeight = 1.0f;

        public Map(IrrlichtDevice d, GameState gs)
        {
            game = gs;
            device = d;
            terrainMesh = StaticMesh.Create();
            //terrainMesh.SetMaterialFlag(MaterialFlag.Lighting, false);
        }
        public void Init(int w, int h)
        {
            width = w;
            height = h;
            tiles = new Node[w,h];
            vertices = new Vertex3D[w+1,h+1];
            indices = new ushort[w,h,6];
            for (int j = 0; j < height+1; j++)
            {
                for (int i = 0; i < width+1; i++)
                {
                    vertices[i,j] = new Vertex3D(new Vector3Df(i, Noise.Generate(i*noiseScale,j * noiseScale)*noiseHeight +noiseOffset, j), new Vector3Df(0.0f,1.0f,0), Color.OpaqueWhite, new Vector2Df(i, j));
                    if (i < width && j < height)
                    {
                        tiles[i, j] = new Node(i, j, 0, 0, null);
                        tiles[i, j].f = 999999;
                        tiles[i, j].cx = i + 0.5f;
                        tiles[i, j].cy = j + 0.5f;
                        int yy = j * (width+1);
                        int yy2 = (j + 1) * (width+1);
                        indices[i, j, 0] = (ushort)(i+yy);
                        indices[i, j, 1] = (ushort)((i+1)+yy);
                        indices[i, j, 2] = (ushort)((i+1)+yy2);
                        indices[i, j, 3] = (ushort)(i+yy);
                        indices[i, j, 4] = (ushort)((i+1)+yy2);
                        indices[i, j, 5] = (ushort)(i+yy2);
                    }
                }
            }
            CalculateNormals();
        }
        public void CalculateNormals()
        {
            for (int j = 0; j < height + 1; j++)
            {
                for (int i = 0; i < width + 1; i++)
                {
                    Vector3Df A = vertices[i, j].Position;
                    Vector3Df R = A;
                    Vector3Df L = A;
                    Vector3Df U = A;
                    Vector3Df D = A;
                    if (i+1 < width + 1)R = vertices[i+1, j].Position;
                    if (i-1 >= 0) L = vertices[i-1, j].Position;
                    if (j+1 < height + 1)D = vertices[i, j+1].Position;
                    if (j-1 >= 0) U = vertices[i, j - 1].Position;
                    Vector3Df n1 = (A - R).CrossProduct(A - D);
                    Vector3Df n2 = (A - L).CrossProduct(A - U);
                    vertices[i, j].Normal = new Vector3Df(Utility.Lerp(n1.X, n2.X, 0.5f), Utility.Lerp(n1.Y, n2.Y, 0.5f), Utility.Lerp(n1.Z, n2.Z, 0.5f)).Normalize().Invert();
                }
            }
        }
        public void BuildMesh()
        {
            terrainMesh.Clear();
            MeshBuffer terrainBuffer = MeshBuffer.Create(VertexType.Standard, IndexType._16Bit);
            Vertex3D[] v = new Vertex3D[(width+1)*(height+1)];
            ushort[] ind = new ushort[width*height*6];
            int counter = 0;
            for (int j = 0; j < height+1; j++)
            {
                for (int i = 0; i < width+1; i++)
                {
                    v[counter] = vertices[i, j];
                    counter += 1;
                }
            }
            counter = 0;
            
            for (int j = 0; j < height; j++)
            {
                for (int i = 0; i < width; i++)
                {
                    for (int k = 5; k >= 0; k--)
                    {
                        ind[counter] = indices[i, j, k];
                        counter += 1;
                    }
                }
            }
            

            terrainBuffer.Append(v, ind);

            terrainBuffer.RecalculateBoundingBox();
            terrainBuffer.SetDirty(HardwareBufferType.VertexAndIndex);
            terrainMesh.AddMeshBuffer(terrainBuffer);
            terrainMesh.SetDirty(HardwareBufferType.VertexAndIndex);
            terrainBuffer.Drop();
            
            terrainMesh.RecalculateBoundingBox();
            GC.Collect();
        }
        public void SetTile(int x, int y, int t)
        {
            tiles[x, y].type = t;
            Color newColor = Color.OpaqueWhite;
            switch (t)
            {
                case 0:
                    newColor = Color.OpaqueWhite;
                    break;
                case 1: //Sand
                    newColor = sandColor;
                    break;
                case 2: //Graveyard
                    newColor = graveyardColor;
                    break;
                case 3: //Shadow
                    newColor = shadowColor;
                    break;
            }
            vertices[x, y].Color = newColor;
            if (x + 1 < width + 1) vertices[x + 1, y].Color = newColor;
            if (y + 1 < height + 1) vertices[x, y + 1].Color = newColor;
            if (x + 1 < width + 1 && y + 1 < height + 1) vertices[x + 1, y + 1].Color = newColor;
        }
        public float getHeightAt(float x, float y)
        {
            if (x >= 0.0f && y > 0.0f && x < width && y < height)
            {
                float xx = x;
                float yy = y;
                int ix = (int)Math.Floor(xx);
                int iy = (int)Math.Floor(yy);
                Vector2Df[] points = new Vector2Df[4];
                points[0] = new Vector2Df(ix, iy);
                points[1] = new Vector2Df(ix + 1, iy);
                points[2] = new Vector2Df(ix + 1, iy + 1);
                points[3] = new Vector2Df(ix, iy + 1);
                float r1 = WeightedAverage(points[1].X, xx, points[0].X, vertices[ix, iy].Position.Y, vertices[ix + 1, iy].Position.Y);
                float r2 = WeightedAverage(points[1].X, xx, points[0].X, vertices[ix, iy + 1].Position.Y, vertices[ix + 1, iy + 1].Position.Y);
                return WeightedAverage(points[0].Y, yy, points[2].Y, r2, r1);
            }
            return 0.0f;
        }
        private float WeightedAverage(float x2, float x, float x1, float Q11, float Q21)
        {
            return ((x2 - x) / (x2 - x1)) * Q11 + ((x - x1) / (x2 - x1)) * Q21;
        }

        //A* pathfinding, folks!


        //Code 1 - Wait
        //Code 2 - Outside Map
        //Code 3 - Too much of map searched
        //Code 4 - Surrounded
        //Code 5 - Redundant
        public List<Node> FindPathTo(int sx, int sy, int ex, int ey, ref int code, int avoidx = -1, int avoidy = -1, bool avoidOthers = false)
        {
            code = 0;
            if (sx == ex && sy == ey)
            {
                code = 5;
                return null;
            }
            bool codeGreen = false;
            int lowestClosed = 999999;
            Node lowestCloseNode = null;
            int ha = (width / 2) * (height / 2);
            if (sx < 0 || sy < 0 || ex < 0 || ey < 0 || sx >= width || sy >= height || ex >= width || ey >= height)
            {
                code = 2;
                return null;
            }
            List<Node> open = new List<Node>();
            List<Node> close = new List<Node>();
            open.Add(new Node(sx, sy, (int)(Utility.DistanceSquared(sx, sy, ex, ey)), 0, null)); //Root node
            open[0].inOpen = true;
            Node currentNode = open[0];
            while (currentNode != tiles[ex, ey])
            {
                int lowest = 999999; //Evaluate current node
                for (int i = 0; i < open.Count; i++)
                {
                    if (open[i].f < lowest)
                    {
                        lowest = open[i].f;
                        currentNode = open[i];
                    }
                }
                currentNode.inOpen = false;
                currentNode.inClosed = true;
                open.Remove(currentNode);
                close.Add(currentNode);
                if (currentNode.h < lowestClosed)
                {
                    lowestClosed = currentNode.h;
                    lowestCloseNode = currentNode;
                }
                //If we have searched half the map and found no path we should stop.
                if (close.Count > ha || codeGreen)
                {
                    code = 3;
                    if (codeGreen) code = 4;
                    currentNode = lowestCloseNode;
                    break;
                }
                if (currentNode == tiles[ex, ey])
                {
                    break;
                }
                //Evaluate surrounding nodes
                int ofsx = -1, ofsy = -1;
                int blockedNeighbors = 0;
                for (int i = 0; i < 8; i++)
                {
                    switch (i) //Move the "cursor" to the appropriate neighbor
                    {
                        case 2:
                        case 1:
                            ofsx += 1;
                            break;
                        case 4:
                        case 3:
                            ofsy += 1;
                            break;
                        case 6:
                        case 5:
                            ofsx -= 1;
                            break;
                        case 7:
                            ofsy -= 1;
                            break;
                    }
                    int xx = currentNode.x + ofsx;
                    int yy = currentNode.y + ofsy;
                    if (xx > 0 && xx < width && yy > 0 && yy < height)
                    {
                        if (xx == avoidx && yy == avoidy) continue;
                        if (tiles[xx, yy].blocked == false && (tiles[xx, yy].occupied == false || avoidOthers == false || (xx == sx && yy == sy)))
                        {
                            if (tiles[xx, yy].inClosed == false)
                            {
                                int gCost = (int)Utility.DistanceSquared(xx, yy, sx, sy);
                                int hCost = (int)Utility.DistanceSquared(xx, yy, ex, ey);
                                int fCost = gCost + hCost;
                                if (fCost < tiles[xx, yy].f || tiles[xx, yy].inOpen == false)
                                {
                                    tiles[xx, yy].f = fCost;
                                    tiles[xx, yy].g = gCost;
                                    tiles[xx, yy].h = hCost;
                                    tiles[xx, yy].parent = currentNode;
                                    if (!tiles[xx, yy].inOpen)
                                    {
                                        tiles[xx, yy].inOpen = true;
                                        tiles[xx, yy].inClosed = false;
                                        open.Add(tiles[xx, yy]);
                                    }
                                }
                            }
                        }
                        else
                        {
                            blockedNeighbors += 1;
                            continue;
                        }
                    }
                    else
                    {
                        continue;
                    }
                }
                if (blockedNeighbors >= 8)
                {
                    codeGreen = true;
                    continue;
                }
            }
            List<Node> r = new List<Node>();
            if (currentNode != null && codeGreen == false)
            {
                //r.Add(currentNode);
                Node nyet = currentNode;
                while (nyet.parent != null)
                {
                    r.Add(nyet);
                    nyet = nyet.parent;
                }
                r.Add(tiles[sx, sy]);
            }
            for (int i = 0; i < Math.Max(open.Count,close.Count); i++)
            {
                if (i < close.Count)
                {
                    close[i].h = close[i].g = close[i].f = 0;
                    close[i].parent = null;
                    close[i].inOpen = false;
                    close[i].inClosed = false;
                }
                if (i < open.Count)
                {
                    open[i].h = open[i].g = open[i].f = 0;
                    open[i].parent = null;
                    open[i].inOpen = false;
                    open[i].inClosed = false;
                }
            }
            
            return r;
        }



        ~Map()
        {
            if (terrainMesh != null) terrainMesh.Drop();
        }
    }
}
