﻿using System;
using System.Collections.Generic;
using System.IO;
using IrrlichtLime;
using IrrlichtLime.Core;
using IrrlichtLime.Scene;
using IrrlichtLime.Video;
using IrrlichtLime.GUI;

namespace Cronikles_Skeleton_War
{
    public class Assets
    {
        public static Dictionary<string, Texture> textures;
        public static Dictionary<string, Mesh> models;
        public static Dictionary<string, AnimatedMesh> animModels;
        public static GUIFont font;
        public static void LoadAssets(IrrlichtDevice device)
        {
            textures = new Dictionary<string, Texture>();
            string[] files = Directory.GetFiles("Assets/Textures/");
            for (int i = 0; i < files.Length; i++)
            {
                if (files[i].Contains(".png"))
                {
                    textures.Add(Path.GetFileNameWithoutExtension(files[i]), device.VideoDriver.GetTexture(files[i]));
                }
            }
            models = new Dictionary<string, Mesh>();
            animModels = new Dictionary<string, AnimatedMesh>();
            files = Directory.GetFiles("Assets/Models/");
            for (int i = 0; i < files.Length; i++)
            {
                if (files[i].Contains(".b3d"))
                {
                    if (files[i].Contains("anim_"))
                    {
                        animModels.Add(Path.GetFileNameWithoutExtension(files[i]).Remove(0, 5), device.SceneManager.GetMesh(files[i]));
                    }
                    else
                    {
                        models.Add(Path.GetFileNameWithoutExtension(files[i]), device.SceneManager.GetMesh(files[i]));
                    }
                }
            }

            font = device.GUIEnvironment.GetFont("Assets/Textures/font.png");
        }
    }
}
