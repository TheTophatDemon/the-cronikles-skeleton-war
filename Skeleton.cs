﻿using System;
using IrrlichtLime;
using IrrlichtLime.Core;
using IrrlichtLime.Scene;
using IrrlichtLime.Video;

namespace Cronikles_Skeleton_War
{
    public class Skeleton : Unit
    {
        static Color passCol = new Color(64, 200, 0);
        static Vector3Df scale = new Vector3Df(0.15f, 0.15f, 0.15f);
        static Vector3Df selBoxOffs = new Vector3Df(0.0f, 5.0f, 0.0f);
        static Vector3Df selBoxScle = new Vector3Df(4.5f, 8.0f, 4.5f);
        Vector3Df selBoxRot = new Vector3Df();
        public bool selected = false;
        public bool aggressive = true;
        private MeshSceneNode selectionBox;
        public string job = "";
        public Human prey;
        int attackTimer = 0;
        int searchTimer = 0;
        //Skeletons will attack only if
            //They are attacked
            //Or
            //They aren't currently doing anything and an enemy comes by
            //Or
            //The user sends them at a specific human
        //Passive skeletons will not attack unless attacked.
        public Skeleton(GameState g) : base(g)
        {
            node = g.sceneManager.AddAnimatedMeshSceneNode(Assets.animModels["skeleton"]);
            node.Scale = scale;
            node.SetFrameLoop(0, 59);
            node.ID = GameState.SELECTABLE;
            node.SetMaterialFlag(MaterialFlag.ColorMaterial, true);
            node.GetMaterial(0).ColorMaterial = ColorMaterial.Ambient;
            selectionBox = g.sceneManager.AddCubeSceneNode(1.0f, node, GameState.NOT_PICKABLE, selBoxOffs);
            selectionBox.Scale = selBoxScle;
            selectionBox.SetMaterialFlag(MaterialFlag.ColorMaterial, false);
            selectionBox.SetMaterialType(MaterialType.TransparentAddColor);
            selectionBox.GetMaterial(0).DiffuseColor = Color.OpaqueBlack;
            selectionBox.GetMaterial(0).EmissiveColor = Color.OpaqueBlack;
            selectionBox.GetMaterial(0).SpecularColor = Color.OpaqueBlack;
            selectionBox.Visible = false;

            newAngle = angle;
        }
        public override void Update()
        {
            Material m = node.GetMaterial(0);
            if (aggressive && m.DiffuseColor != Color.OpaqueWhite)
            {
                m.DiffuseColor = Color.OpaqueWhite;
            }
            if (!aggressive && m.DiffuseColor != passCol)
            {
                m.DiffuseColor = passCol;
            }
            if (state == "die")
            {
                if (selected)
                {
                    selected = false;
                    game.selectedSkeletons.Remove(this);
                }
                if (node.CurrentFrame >= node.EndFrame - 2) {
                    node.CurrentFrame = node.EndFrame - 2;
                    node.AnimationSpeed = 0;
                }
            }
            else
            {
                switch (job)
                {
                    case "":
                        prey = null;
                        if (state == "idle" && aggressive)
                        {
                            searchTimer += 1;
                            if (searchTimer > 10)
                            {
                                prey = FindNearbyHuman();
                                if (prey != null) SetJob("attack");
                            }
                        }
                        break;
                    case "attack":
                        if (path == null || prey == null)
                        {
                            SetJob("");
                            break;
                        }
                        if (prey != null)
                        {
                            if (state == "attack")
                            {
                                FaceUnit(prey);
                                attackTimer += 1;
                                if (attackTimer > 25)
                                {
                                    prey.Damage(damage, this);
                                    attackTimer = 0;
                                }
                            }
                            if (Math.Abs(prey.mapPosition.X - mapPosition.X) <= 1 && Math.Abs(prey.mapPosition.Y - mapPosition.Y) <= 1) //If close enough, start attacking
                            {
                                if (state == "idle") ChangeState("attack");
                            }
                            else //Otherwise move closer
                            {
                                if ((state != "walk" && state != "wait") || (dest.X != prey.mapPosition.X && dest.Y != prey.mapPosition.Y)) MoveTo(prey.mapPosition.X, prey.mapPosition.Y);
                            }
                            if (prey.killMe || prey.state == "die") prey = null;
                        }
                        break;
                }

                if (selected)
                {
                    if (state == "idle") newAngle = (float)-(Math.Atan2(position.Z - game.camera.Position.Z, position.X - game.camera.Position.X) * (180.0f / Math.PI)) + 90.0f;
                    selectionBox.Visible = true;
                    selectionBox.Rotation = selBoxRot;
                    selBoxRot.Y += 2f;
                }
                else
                {
                    selectionBox.Visible = false;
                }
            }

            base.Update();

            if (state == "idle")
            {
                if (pstate != "idle") node.SetFrameLoop(0, 59);
            }
            else if (state == "die")
            {
                if (pstate != "die") node.SetFrameLoop(139, 170);
            }
            else if (state == "attack")
            {
                if (pstate != "attack") node.SetFrameLoop(89, 129);
            }
            else if (position != lastPosition)
            {
                if (node.StartFrame != 65) node.SetFrameLoop(65, 85); //Walk animation
            }
            else
            {
                if (node.StartFrame != 0) node.SetFrameLoop(0, 59);
            }
            pstate = state;
        }
        public override void OnUnitHit(Unit perpetrator)
        {
            base.OnUnitHit(perpetrator);
            //pathBlocked = true;
        }
        public void SetJob(string newjob)
        {
            if (job == "attack" && newjob != "attack" && state == "attack") ChangeState("idle");
            switch (newjob)
            {
                case "attack":
                    if (prey != null)
                    {
                        MoveTo(prey.mapPosition.X, prey.mapPosition.Y);
                    }
                    else
                    {
                        Console.WriteLine("SOMETHINS WRONG HERE MATEY");
                        return;                        
                    }
                    break;
            }
            job = newjob;
        }
        public override void Damage(int amount, Unit perpetrator)
        {
            base.Damage(amount, perpetrator);
            if (perpetrator is Human && perpetrator.health > 0)
            {
                if (prey == null) prey = (Human)perpetrator;
                if (job != "attack")
                {
                    if (state == "idle" || aggressive)
                    {
                        SetJob("attack");
                    }
                }
            }
        }
        public override void PostUpdate()
        {
            base.PostUpdate();
            if (killMe)
            {
                game.skeletons.Remove(this);
                game.selectedSkeletons.Remove(this);
            }
        }
        public Human FindNearbyHuman(int range = 4)
        {
            for (int i = 0; i < game.units.Count; i++)
            {
                if (game.units[i] is Human && game.units[i].killMe == false && game.units[i].state != "die")
                {
                    int dx = Math.Abs(game.units[i].mapPosition.X - mapPosition.X);
                    int dy = Math.Abs(game.units[i].mapPosition.Y - mapPosition.Y);
                    if (dx <= range && dy <= range)
                    {
                        return (Human)game.units[i];
                    }
                }
            }
            return null;
        }
    }
}
