﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IrrlichtLime;
using IrrlichtLime.Core;

namespace Cronikles_Skeleton_War
{
    public class Utility
    {
        //These are here to minimize memory usage

        public static Vector3Df vNull = new Vector3Df(-3.14f, 42.0f, 666.666f); //Can't set Vectors to null for some reason, so this is the workaround.
        public static Vector3Df v3zero = new Vector3Df();
        public static Vector2Di v2done = new Vector2Di(1,1);
        public static Vector2Df v2half = new Vector2Df(0.5f, 0.5f);

        public static Vector3Df treeSize = new Vector3Df(0.4f, 0.4f, 0.4f);
        public static float toDegrees = (float)(180.0f / Math.PI);
        public static float toRadians = (float)(Math.PI / 180.0f);

        public static float Lerp(float v0, float v1, float w)
        {
            return (1 - w) * v0 + w * v1;
        }
        public static float Distance(float x, float y, float xx, float yy)
        {
            return (float)Math.Sqrt((x-xx)*(x-xx)+(y-yy)*(y-yy));
        }
        public static float DistanceSquared(float x, float y, float xx, float yy)
        {
            return (float)(x - xx) * (x - xx) + (y - yy) * (y - yy);
        }
        public static float ManhattanDistance(float x, float y, float xx, float yy)
        {
            //return (float)((x - xx) * (x - xx) + (y - yy) * (y - yy));
            return Math.Abs(x-xx) + Math.Abs(y-yy);
        }
    }
}
