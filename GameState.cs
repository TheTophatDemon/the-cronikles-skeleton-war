﻿using System;
using IrrlichtLime;
using IrrlichtLime.Core;
using IrrlichtLime.Scene;
using IrrlichtLime.Video;
using IrrlichtLime.GUI;
using System.Collections.Generic;

namespace Cronikles_Skeleton_War
{
    //The ultimate solution: Separate what tells you when the unit has finished a path and the idle state.


    //Health bars appear when hovering over units
    //Consider fixing post and pre update
    //Make them sparkle when attacking
    
    //Add shadow gradients to tree textures

    public class GameState : State
    {
        static readonly Color skyColor = new Color(0, 0, 64);
        Vector3Df lightDirection = new Vector3Df(0.0f, 0.0f, 0.0f);
        Vector3Df cameraTargetOffset = new Vector3Df(0.0f, -1.0f, 1.0f);
        Dimension2Df particleSize = new Dimension2Df(0.2f, 0.2f);
        GUIStaticText fpsCounter;
        GUIStaticText tileInfo;
        GUIImage infoBar;
        GUIImage hoverIcon;
        GUIButton ibAgress;
        GUIButton ibPass;
        static Recti ibCount = new Recti(96, 536, 128, 64);
        bool tBlocc;
        int tx;
        int ty;

        public const int NOT_PICKABLE = 0;
        public const int SELECTABLE = 1;
        public const int TERRAIN = 1 << 1;
        public const int BUILDING = 1 << 2;
        public const int ENEMY = 1 << 3;

        public Map gameMap;
        public MeshSceneNode mapNode;
        public CameraSceneNode camera;
        private Vector3Df cameraPosition;
        private Vector3Df cameraSpeed;
        private LightSceneNode light;
        private MeshSceneNode selectionPlane;
        private Vector3Df spPos = new Vector3Df();
        private Vector3Df spScl = new Vector3Df();
        private SceneNode skybox;
        private Vector3Df skyRot = new Vector3Df();

        public List<Unit> units;
        public List<Skeleton> skeletons;     //MAKE SURE TO ADD/REMOVE SKELETONS TO BOTH THE SKELETONS LIST AND THE UNITS LIST
        
        public List<Skeleton> selectedSkeletons;
        private bool selecting = false;
        private bool rectSelecting = false;
        Vector2Di cursor = new Vector2Di();
        Vector2Di selectStart = new Vector2Di();
        Vector2Di selectEnd = new Vector2Di();
        bool js;
        
        public List<Building> buildings;
        public int skelPathCnt = 0;
        public int humPathCnt = 0;

        public int globalTimer = 0;

        private ParticleSystemSceneNode clickParticles;

        public GameState(IrrlichtDevice d, SceneManager sm, GUIEnvironment g, VideoDriver vd) : base(d, sm, g, vd)
        {
            units = new List<Unit>();
            skeletons = new List<Skeleton>();
            selectedSkeletons = new List<Skeleton>();
            fpsCounter = gui.AddStaticText("FPS: ", new Recti(0, 0, 400, 32));
            fpsCounter.OverrideColor = Color.OpaqueWhite;
            fpsCounter.OverrideFont = Assets.font;
            tileInfo = gui.AddStaticText("TILE INFO:", new Recti(0, 32, 200, 400));
            tileInfo.OverrideColor = Color.OpaqueWhite;
            
        }
        public override void Init()
        {
            
            units.Clear();
            skeletons.Clear();
            sceneManager.Clear();

            //GC.Collect();

            sceneManager.AmbientLight = new Colorf(0.5f, 0.5f, 0.5f);
            camera = sceneManager.AddCameraSceneNode();

            cameraPosition = new Vector3Df(0.0f, 10.0f, 0.0f);

            light = sceneManager.AddLightSceneNode();
            light.Rotation = lightDirection;
            light.LightType = LightType.Directional;

            selectionPlane = sceneManager.AddCubeSceneNode(2.0f);
            selectionPlane.Visible = false;
            selectionPlane.SetMaterialTexture(0, Assets.textures["selection"]);
            selectionPlane.GetMaterial(0).Type = MaterialType.TransparentAlphaChannel;
            selectionPlane.GetMaterial(0).SetFlag(MaterialFlag.BackFaceCulling, false);
            selectionPlane.GetMaterial(0).SetFlag(MaterialFlag.Lighting, false);

            InitHud();

            clickParticles = sceneManager.AddParticleSystemSceneNode(true, null, NOT_PICKABLE);
            clickParticles.GetMaterial(0).SetTexture(0, Assets.textures["particle0"]);
            ParticleSphereEmitter pse = clickParticles.CreateSphereEmitter(new Vector3Df(), 5f);
            clickParticles.Emitter = pse;
            pse.Drop();

            clickParticles.Scale = new Vector3Df(0.1f, 0.01f, 0.1f);
            clickParticles.SetParticleSize(particleSize);
            clickParticles.Emitter.MinParticlesPerSecond = clickParticles.Emitter.MaxParticlesPerSecond = 15;
            clickParticles.Emitter.Direction = new Vector3Df(0.0f, 0.5f, 0.0f);
            clickParticles.Emitter.MaxLifeTime = clickParticles.Emitter.MinLifeTime = 500;
            clickParticles.SetMaterialFlag(MaterialFlag.Lighting, false);
            clickParticles.SetMaterialType(MaterialType.TransparentAlphaChannel);
            clickParticles.Emitter.MinStartColor = clickParticles.Emitter.MaxStartColor = Color.OpaqueWhite;
            
            clickParticles.Visible = false;

            SetupMap();

            skybox = sceneManager.AddSkyDomeSceneNode(Assets.textures["sky"], 16, 16, 0.5f, 1, 1.0f);
            skybox.GetMaterial(0).SetTextureMatrix(0, new Matrix(Utility.v3zero, Utility.v3zero, new Vector3Df(4, 4, 4)));
            skybox.Scale = new Vector3Df(gameMap.width * 2, gameMap.width * 2, gameMap.width * 2);
            skybox.Rotation = new Vector3Df(180.0f, 0.0f, 0.0f);
            skyRot.X = 180.0f;

            for (int i = 0; i < 50; i++)
            {
                Skeleton skeltal = new Skeleton(this);
                skeltal.position = new Vector3Df(Program.random.Next(0, 63) + 0.5f, 0.0f, Program.random.Next(0, 31) + 0.5f);
                skeltal.newAngle = skeltal.angle = (float)Program.random.NextDouble() * 360.0f;
                skeletons.Add(skeltal);
                units.Add(skeltal);
            }

        }
        public override void Update()
        {
            globalTimer += 1;
            skyRot.Y += 0.2f;
            skybox.Rotation = skyRot;
            ControlCamera();
            Vector3Df colPnt = Utility.vNull;
            Triangle3Df colTri = new Triangle3Df();
            sceneManager.SceneCollisionManager.GetCollisionPoint(sceneManager.SceneCollisionManager.GetRayFromScreenCoordinates(new Vector2Di(Program.MOUSE_X, Program.MOUSE_Y)), mapNode.TriangleSelector, out colPnt, out colTri);
            if (colPnt != Utility.vNull)
            {
                cursor.X = (int)Math.Floor(colPnt.X);
                cursor.Y = (int)Math.Floor(colPnt.Z);

            }
            SceneNode colNode = sceneManager.SceneCollisionManager.GetSceneNodeFromScreenCoordinatesBB(new Vector2Di(Program.MOUSE_X, Program.MOUSE_Y), SELECTABLE);
            if (Program.MOUSE_LEFT && (Program.MOUSE_Y < 504 || infoBar.Visible == false))
            {
                if (!selecting)
                {
                    if (selectedSkeletons.Count > 0)
                    {
                        for (int i = 0; i < selectedSkeletons.Count; i++)
                        {
                            selectedSkeletons[i].selected = false;
                        }
                        selectedSkeletons.Clear();
                    }
                    if (Program.KEY_SHIFT)
                    {
                        rectSelecting = true;
                        selectStart.X = cursor.X;
                        selectStart.Y = cursor.Y;
                        selectEnd = selectStart;
                    }
                }
                selecting = true;
                if (rectSelecting)
                {
                    selectionPlane.Visible = true;

                    float xd = Math.Abs(selectStart.X - selectEnd.X);
                    float zd = Math.Abs(selectStart.Y - selectEnd.Y);
                    spScl.X = xd / 2;
                    spScl.Y = 4.0f;
                    spScl.Z = zd / 2;
                    selectionPlane.Scale = spScl;

                    float cx = (selectStart.X + selectEnd.X) / 2.0f;
                    float cy = (selectStart.Y + selectEnd.Y) / 2.0f;
                    spPos.X = cx;
                    spPos.Z = cy;
                    spPos.Y = 0.0f;
                    selectionPlane.Position = spPos;
                    
                    selectEnd.X = cursor.X;
                    selectEnd.Y = cursor.Y;
                }
                else
                {
                    
                    
                    if (colNode != null)
                    {
                        for (int i = 0; i < skeletons.Count; i++)
                        {
                            if (skeletons[i].node == colNode && skeletons[i].selected == false && skeletons[i].state != "die")
                            {
                                selectedSkeletons.Add(skeletons[i]);
                                skeletons[i].selected = true;
                            }
                        }
                    }
                }
            }
            else
            {
                selectionPlane.Visible = false;
                if (js && rectSelecting)
                {
                    int left = Math.Min(selectEnd.X, selectStart.X);
                    int right = Math.Max(selectEnd.X, selectStart.X);
                    int top = Math.Min(selectEnd.Y, selectStart.Y);
                    int bottom = Math.Max(selectEnd.Y, selectStart.Y);
                    for (int i = 0; i < skeletons.Count; i++)
                    {
                        if (skeletons[i].selected == false)
                        {
                            if (skeletons[i].mapPosition.X >= left && skeletons[i].mapPosition.X < right && skeletons[i].mapPosition.Y >= top && skeletons[i].mapPosition.Y < bottom && skeletons[i].state != "die")
                            {
                                selectedSkeletons.Add(skeletons[i]);
                                skeletons[i].selected = true;
                            }
                        }
                    }
                }
                selecting = false;
                rectSelecting = false;

            }
            //hoverIcon.Visible = false;
            //ibAgress.Visible = false;
            infoBar.Visible = false;
            if (selectedSkeletons.Count > 0)
            {
                infoBar.Visible = true;
                //hoverIcon.Visible = true;
                //ibAgress.Visible = true;
            }
            js = Program.MOUSE_LEFT;

            skelPathCnt = 0;
            humPathCnt = 0;
            if (Program.MOUSE_RIGHT_CLICK)
            {
                if (colPnt != Utility.vNull)
                {
                    for (int i = 0; i < selectedSkeletons.Count; i++)
                    {
                        selectedSkeletons[i].SetJob("");
                    }
                    selecting = false;
                    float radius = (float)Math.Ceiling(Math.Sqrt(selectedSkeletons.Count) / 2.0f);
                    int cx = (int)Math.Floor(colPnt.X);
                    int cy = (int)Math.Floor(colPnt.Z);
                    Human target = null;
                    if (colNode != null)
                    {
                        for (int i = 0; i < units.Count; i++)
                        {
                            if (units[i] is Human && units[i].node == colNode)
                            {
                                target = (Human)units[i];
                                break;
                            }
                        }
                    }
                    if (target != null)
                    {
                        for (int i = 0; i < selectedSkeletons.Count; i++)
                        {
                            selectedSkeletons[i].prey = target;
                            selectedSkeletons[i].SetJob("attack");
                        }
                    }
                    else if (gameMap.tiles[cx, cy].blocked)
                    {
                        for (int i = 0; i < selectedSkeletons.Count; i++)
                        {
                            selectedSkeletons[i].MoveTo(cx,cy);
                        }
                    }
                    else
                    {

                        float centX = 0.0f;
                        float centY = 0.0f;
                        for (int i = 0; i < selectedSkeletons.Count; i++)
                        {
                            centX += selectedSkeletons[i].position.X;
                            centY += selectedSkeletons[i].position.Z;
                        }
                        centX /= selectedSkeletons.Count;
                        centY /= selectedSkeletons.Count;
                        for (int i = 0; i < selectedSkeletons.Count; i++)
                        {
                            int difx = (int)Math.Floor(selectedSkeletons[i].position.X - centX);
                            int dify = (int)Math.Floor(selectedSkeletons[i].position.Z - centY);
                            selectedSkeletons[i].MoveTo(Math.Min(gameMap.width - 1, Math.Max(0, cx + difx)), Math.Min(gameMap.height - 1, Math.Max(0, cy + dify)));
                        }
                    }
                    
                    colPnt.X = (float)Math.Floor(colPnt.X) + 0.5f;
                    colPnt.Z = (float)Math.Floor(colPnt.Z) + 0.5f;
                    clickParticles.Position = colPnt;
                    clickParticles.Visible = true;

                    

                    //GC.Collect(); //Get rid of all that pathfinding memory!
                }
            }
            for (int i = 0; i < skeletons.Count; i++)
            {
                if (Program.KEY_Y)
                {
                    skeletons[i].MoveTo(32, 32);
                }
            }

            for (int j = 0; j < gameMap.height; j++)
            {
                for (int i = 0; i < gameMap.width; i++)
                {
                    gameMap.tiles[i,j].occupied = false;
                    gameMap.tiles[i, j].occupant = null;
                }
            }
            for (int i = 0; i < units.Count; i++)
            {
                units[i].PreUpdate();
                units[i].Update();
                units[i].PostUpdate();
            }
            // if (pathCnt > 0) Console.WriteLine("PATHS FOUNDED: " + pathCnt);

            if (Program.KEY_H && globalTimer % 10 == 0)
            {
                AddHuman();
            }
            if (colPnt != Utility.vNull)
            {
                Node n = gameMap.tiles[Math.Min(63, Math.Max(0, (int)Math.Floor(colPnt.X))), Math.Min(63, Math.Max(0, (int)Math.Floor(colPnt.Z)))];
                //if (n.reservedFor != null) tRes = true;
                tx = n.x;
                ty = n.y;
                tBlocc = n.blocked;
                tileInfo.Text = "TILE INFO:" + "\nBlocked" + tBlocc + "\nX:" + tx + "\nY:" + ty;
            }
            for (int i = 0; i < buildings.Count; i++)
            {
                buildings[i].Update();
            }
            fpsCounter.Text = "FPS: " + driver.FPS;
        }
        private void ControlCamera()
        {
            if (Program.KEY_UP)
            {
                cameraSpeed.Z += 0.05f;
                if (cameraSpeed.Z > 0.3f) cameraSpeed.Z = 0.3f;
            }
            else if (Program.KEY_DOWN)
            {
                cameraSpeed.Z -= 0.05f;
                if (cameraSpeed.Z < -0.3f) cameraSpeed.Z = -0.3f;
            }
            else
            {
                cameraSpeed.Z *= 0.9f;
                if (Math.Abs(cameraSpeed.Z) < 0.001f) cameraSpeed.Z = 0.0f;
            }
            if (Program.KEY_RIGHT)
            {
                cameraSpeed.X += 0.05f;
                if (cameraSpeed.X > 0.3f) cameraSpeed.X = 0.3f;
            }
            else if (Program.KEY_LEFT)
            {
                cameraSpeed.X -= 0.05f;
                if (cameraSpeed.X < -0.3f) cameraSpeed.X = -0.3f;
            }
            else
            {
                cameraSpeed.X *= 0.9f;
                if (Math.Abs(cameraSpeed.X) < 0.001f) cameraSpeed.X = 0.0f;
            }
            if (Program.KEY_Q)
            {
                cameraPosition.Z -= 0.3f;
                cameraPosition.Y += 0.3f;
            }
            else if (Program.KEY_E)
            {
                if (cameraPosition.Y > 4.0f)
                {
                    cameraPosition.Z += 0.3f;
                    cameraPosition.Y -= 0.3f;
                }
            }
            
            cameraPosition += cameraSpeed;
            camera.Position = cameraPosition;
            camera.Target = camera.Position + cameraTargetOffset;
        }
        public override void Render()
        {
            driver.BeginScene(ClearBufferFlag.Color | ClearBufferFlag.Depth,skyColor);

            sceneManager.DrawAll();
            gui.DrawAll();
            if (hoverIcon.Visible && selectedSkeletons.Count > 0)
            {
                Assets.font.Draw("x" + selectedSkeletons.Count, ibCount, Color.OpaqueWhite, false);
                Assets.font.Draw("AGGRESSIVE", 96, 568, (ibAgress.Pressed) ? Color.OpaqueBlue : Color.OpaqueWhite);
                Assets.font.Draw("PASSIVE", 144, 536, (ibPass.Pressed) ? Color.OpaqueBlue : Color.OpaqueWhite);
                //144 536 304 552
            }

            driver.EndScene();
        }
        public override void HandleEvent(Event e)
        {
            if (e.GUI.Caller == ibAgress)
            {
                if (e.GUI.Type == GUIEventType.ButtonClicked)
                {
                    if (selectedSkeletons.Count > 0)
                    {
                        for (int i = 0; i < selectedSkeletons.Count; i++)
                        {
                            selectedSkeletons[i].aggressive = true;
                        }
                    }
                }
            }
            else if (e.GUI.Caller == ibPass)
            {
                if (e.GUI.Type == GUIEventType.ButtonClicked)
                {
                    if (selectedSkeletons.Count > 0)
                    {
                        for (int i = 0; i < selectedSkeletons.Count; i++)
                        {
                            selectedSkeletons[i].aggressive = false;
                        }
                    }
                }
            }
        }
        public void AddHuman(float x = -1, float y = -1)
        {
            Human human = new Human(this);
            human.position = new Vector3Df(x, 0.0f, y);
            if (x == -1) human.position.X = Program.random.Next(0, 63) + 0.5f;
            if (y == -1) human.position.Z = Program.random.Next(36, 63) + 0.5f;
            human.target.X = (int)Math.Floor(human.position.X);
            human.target.Y = (int)Math.Floor(human.position.Z);
            human.ChangeState("walk2");
            human.aiState = "wander";
            units.Add(human);
        }
        public void InitHud()
        {
            infoBar = gui.AddImage(Assets.textures["hud_thing"], new Vector2Di(0, 504));
            hoverIcon = gui.AddImage(Assets.textures["skeleton_icon"], new Vector2Di(16, 16), false, infoBar);
            ibAgress = gui.AddButton(new Recti(96, 64, 256, 80), infoBar, -1);
            ibAgress.SetImage(Assets.textures["button"], new Recti(0, 0, 160, 16));
            ibPass = gui.AddButton(new Recti(144, 32, 256, 48), infoBar, -1);
            ibPass.SetImage(Assets.textures["button"], new Recti(0, 16, 112, 32));
        }
        public void SetupMap()
        {
            gameMap = new Map(device, this);
            gameMap.Init(64, 64);
            gameMap.SetTile(2, 2, 1);
            gameMap.BuildMesh();
            mapNode = sceneManager.AddMeshSceneNode(gameMap.terrainMesh, null, TERRAIN);
            mapNode.SetMaterialTexture(0, Assets.textures["grass"]);
            mapNode.SetMaterialFlag(MaterialFlag.Lighting, true);
            mapNode.SetMaterialFlag(MaterialFlag.ColorMaterial, true);
            mapNode.GetMaterial(0).ColorMaterial = ColorMaterial.Ambient;

            TriangleSelector selector = sceneManager.CreateOctreeTriangleSelector(gameMap.terrainMesh, mapNode);
            mapNode.TriangleSelector = selector;
            selector.Drop();

            buildings = new List<Building>();
            //Get those trees out, man!
            for (int i = 0; i < 100; i++)
            {
                Building b = new Building(this, new Vector2Di(Program.random.Next(64), Program.random.Next(64)), Utility.v2done, Assets.models["tree"]);
                b.node.Scale = Utility.treeSize;
                b.node.Rotation = new Vector3Df(0.0f, (float)Program.random.NextDouble() * 360.0f, 0.0f);
                buildings.Add(b);
                
            }
            gameMap.BuildMesh();
        }
    }
    
}
