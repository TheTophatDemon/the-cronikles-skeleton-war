﻿using System;
using IrrlichtLime;
using IrrlichtLime.Core;
using IrrlichtLime.Scene;
using IrrlichtLime.Video;
using IrrlichtLime.GUI;

namespace Cronikles_Skeleton_War
{
    public abstract class State
    {
        public IrrlichtDevice device;
        public SceneManager sceneManager;
        public GUIEnvironment gui;
        public VideoDriver driver;
        public State(IrrlichtDevice d, SceneManager sm, GUIEnvironment g, VideoDriver vd)
        {
            device = d;
            sceneManager = sm;
            gui = g;
            driver = vd;
        }
        public abstract void Init();
        public abstract void Update();
        public abstract void Render();
        public abstract void HandleEvent(Event e);
    }
}
