﻿using System;
using IrrlichtLime;
using IrrlichtLime.Core;
using IrrlichtLime.Scene;
using IrrlichtLime.Video;

namespace Cronikles_Skeleton_War
{
    public class Building //Map bound structures that impede unit movement
    {
        public MeshSceneNode node;
        public Vector2Di tilePosition;
        public Vector2Di tileSize;
        public int health = 100;
        protected GameState game;
        protected Vector3Df position;
        public Building(GameState g, Vector2Di tp, Vector2Di ts, Mesh mesh)
        {
            game = g;
            tilePosition = tp;
            tileSize = ts;
            for (int j = 0; j < tileSize.Y; j++)
            {
                for (int i = 0; i < tileSize.X; i++)
                {
                    g.gameMap.tiles[tilePosition.X+i, tilePosition.Y+j].blocked = true;
                    g.gameMap.SetTile(tilePosition.X + i, tilePosition.Y + j, 3);
                }
            }
            node = g.sceneManager.AddMeshSceneNode(mesh, null, GameState.BUILDING);
            position = new Vector3Df(tilePosition.X + (tileSize.X / 2.0f), 0.0f, tilePosition.Y + (tileSize.Y / 2.0f));
            position.Y = g.gameMap.getHeightAt(position.X, position.Z);
        }
        public virtual void Update()
        {
            node.Position = position;
        }
        public void Destroy()
        {
            if (game != null)
            {
                if (game.gameMap != null)
                {
                    game.buildings.Remove(this);
                    for (int j = 0; j < tileSize.Y; j++)
                    {
                        for (int i = 0; i < tileSize.X; i++)
                        {
                            game.gameMap.tiles[tilePosition.X + i, tilePosition.Y + j].blocked = false;
                        }
                    }
                }
            }
        }
        ~Building()
        {
            Destroy();
        }
    }
}
